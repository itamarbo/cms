<?php

use App\Post;
use App\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   
    return view('welcome');
});

/*
Route::get('/post/{id}/{name}', function ($id,$name) {
  
    return "Hi post number ". $id . " " . $name;
});

Route::get('admin/posts/example', array('as'=>'admin.home',function (){
    $url = route('admin.home');
    
    return "This url is " . $url;
}));
*/

//Route::get('/post/{id}', 'PostsController@index');

//Route::resource('posts', 'PostsController');

//Route::get('/contact', 'PostsController@contact');

//Route::get('post/{id}', 'PostsController@show_post');

/*

Route::get('/insert', function(){

    DB::insert('insert into posts(title, content) values(?, ?)', ['Laravel is awesome wit Edwin', 'PERIOD']);

});



Route::get('/read', function() {
   $results = DB::select('select * from posts where id = ?', [2]);
    return var_dump($results);



});


Route::get('/update', function(){


        $updated = DB::update('update posts set title = "Update title" where id = ?', [1]);

        return $updated;


});


Route::get('/delete', function(){


    $deleted = DB::delete('delete from posts where id = ?', [1]);
    return $deleted;
    


});


Route::get('/read', function(){

    $posts = Post::all(); // שולף את כל הרשומות מטבלת פוסטס
    foreach($posts as $post){

        return $post->title;
        
    }


});


Route::get('/find', function(){

    $posts = Post::find(2); 
    return $posts->title;
    
});



Route::get('/findwhere', function(){

    $posts = Post::where('id',2)->orderBy('id', 'desc')->take(1)->get();
    return $posts;
    
});


Route::get('/findmore', function(){

    // $posts = Post::findOrFail(1);
    // return $posts;
    $posts = Post::from('posts', '<', 50)->firstOrFail();
    return $posts;
});


Route::get('/basicinsert', function(){

    $post = new Post;
    $post->title = 'New Eloquent title insert';
    $post->content = 'Wow Eloquent is really cool, look at it';
    $post->save();
});



Route::get('/basicinsert2', function(){

    $post = Post::find(2);
    $post->title = 'New Eloquent title insert 2';
    $post->content = 'Wow Eloquent is really cool, look at this content 2';
    $post->save();
   
});


Route::get('/create', function(){


    Post::create(['title'=>'the create method 2', 'content'=>'Wow im learning alot with Edwin 2']);

});



Route::get('/update', function(){


    Post::where('id', 2)->where('is_admin', 0)->update(['title'=>'NEW PHP TITLE', 'content'=>'I love my boo']);

});


Route::get('/delete', function(){

    $post = Post::find(3);
    $post->delete();

});


Route::get('/delete2', function(){

    Post::destroy([5,6]);

//    Post::where('is_admin', 0)->delete();;
});


Route::get('/softdelete', function(){

    Post::find(2)->delete();

});


Route::get('/readsoftdelete', function(){

    //$post = Post::withTrashed()->where('is_admin', 0)->get();
    //return $post;

       $post = Post::onlyTrashed()->where('id', 1)->get();
       return $post;
});


Route::get('/restore', function(){

    Post::withTrashed()->where('is_admin', 0)->restore();

});


Route::get('/forcedelete', function(){

    Post::onlyTrashed()->where('is_admin', 0)->forcedelete();
});


// one to one relationship
Route::get('/user/{id}/post', function($id){

    return User::find($id)->post->content;

});


Route::get('/post/{id}/user', function($id){

    return Post::find($id)->user->name;

});


// one to many relationship
Route::get('/posts', function(){

    $user = User::find(1);

    foreach($user->posts as $post){
      echo  $post->title ."<br>";
    
    }
});
*/
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/post/{id}', 'PostsController@index');
Route::resource('posts', 'PostsController');