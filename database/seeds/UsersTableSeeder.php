<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'itamar',
            'email' => 'itamar@gmail.com',
            'password' => bcrypt('secret'),
        ]);
    }
}
